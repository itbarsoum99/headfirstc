# Head First C - Notes

[Book](https://www.amazon.com/Head-First-C-Brain-Friendly-Guide/dp/1449399916)<br/>
[Errata](https://www.oreilly.com/catalog/errata.csp?isbn=0636920015482)


## Chapter 1: Getting Started with C

#### What can C do?

* High performance code for a new game
* Program an Arduino
* Use third-party library on iPhone
* Lower level than other languages (creates code that's a lot closer to
  what machines really understand)
* Uses a compiler
* Designed to create smalll, fast programs
* Most operating systems, computer languages, and game software are
  written in C

#### The way C works

1. Source code
2. Compiler (gcc)
3. Output (gcc creates binary code)


#### Complete C programs

* Begin with a comment
  * Includes the purpose of the code and includes license/copyright
    information

```c
/*
 * This is a comment
 * Second line of comment
 * (c)2020 comments
 */
```
* Include section
  * External libraries are necessary for C, as it is otherwise a very
    small language

```c
#include <stdio.h>
```

* Functions
  * All C code runs inside functions
  * `main()` is the starting point for all code

```c
int main() {
  int decks = 0;
  if (decks < 1) {
    printf("hello, world\n");
  } else if (decks > 1) {
    printf("goodbye, world\n");
  } else {
    printf("welcome, world\n");
  }
}
```

#### The main() function

* Program cannot start without a main function
* The `main()` function has a return type of `int`.
  * When the computer runs the program, it checks the return value of
    the `main()` function.
  * If you tell the `main()` function to `return 0`, that indicates
    that the function ran successfully. If you tell it to `return`
    anything else, that indicates there was a problem
* The function body should always be surrounded by braces.

```c
int main() {
/* Return type - always int for the main() function */
  int decks;
  puts("Enter a number of decks");
  scanf("%i", &decks);
  if (decks < 1) {
    puts("That is not a valid number of decks");
    return 1;
  }
  printf("There are %i cards\n", (decks * 52));
  return 0;
}
```

#### The printf() function

* Used to display formatted output
* Replaces format characters with the values of variables
* [Complete list of C conversion specifiers](https://flaviocopes.com/c-conversion-specifiers/)

```c
printf("%s says the count is %i", "Ben", 21);
/* Tells the printf() function that a string and an integer are coming in that order, then gives them at the end. */
```

#### How to run the program

* Need to convert ("compile") the source code into machine code.
* Use a compiler like [gcc](http://gcc.gnu.org/)

##### Steps

* Save the code on a file (e.g. `hello.c`)
* Compile with gcc and run

```bash
> gcc -o hello hello.c
>
> ./hello
Hello, World!
>
```

#### Strings

* C doesn't support strings out of the box, but there are a number of
  extension libraries that do give you strings
* Instead of using strings, it uses an array of single characters.
* Strings are just character arrays
* To C, `str = "Shatner"` is the same as `s = {'S', 'h', 'a', 't', 'n', 'e', 'r'}`
* Strings end in a null character, so you have to define the variable
  with an extra character allowance (`\0` ends the string)

#### Equal Signs

* The equals sign (=) is used for assignment
* The double equals sign (==) is used for testing equality

```c
int main() {
  int s = 1;
  if (s == 1) {
    printf("Hello, world");
  }
}
```

* To increase or decrease a variable:

```c
int main() {
  int s = 0;
  if (s == 0) {
    s += 1;
    continue;
  } else if (s == 1) {
    printf("hello, world");
  }
}
```

* To increase or decrease a variable by 1:
```c
int main() {
  int s = 2;
  if (s == 2) {
    s --;
    continue;
  } else if (s == 1) {
    printf("hello, world");
  }
}
```

#### Types of commands

1. Do something

* Simple statements: actions

```c
split_hand();
```

* Block statements: group

```c
{
  deal_first_card();
  deal_second_card();
  cards_in_hand = 2;
}
```

2. Do something only if something is true

* Control statements such as `if` check a condition before running the
  code

```c
if (value_of_hand <= 16)
  hit();
else 
  stand();
```

* `if` often needs to do more than one thing (so they use block
  statements):

```c
if (dealer_card == 6) {
  double_down();
  hit();
}
```

#### More to booleans than equals

* `&&` checks if two conditions are true:

```c
if ((dealer_up_card == 6) && (hand == 11))
  double_down();
```

* `||` checks if either condition is true:

```c
if (cupcakes_in_fridge || chips_on_table)
  eat_food();
```

* `!` flips the value of a condition

```c
if (!brad_on_phone)
  answer_phone();
/* Checks if Brad is NOT on the phone, and executes if he is not */
```

#### Switch statement

* Like an `if` statement, but can test for multiple values instead of
  a single variable.
* Checks the value it was given, then looks for matching case. When it
  finds one, it runs all of the code after it until a `break`
  statement.

```c
switch(train) {
case 37:
  winnings = winnings + 50;
  break;
case 65:
  puts("Jackpot!");
  winnings = winnings + 80;
  /* Add 80, then do not break, and add 20 (below) */
case 12:
  winnings = winnings + 20;
  break;
default:
  winnings = 0;
  /* For any other value, set the winnings to 0 */
}
```

#### While statement

* Special type of control statement
* Most basic kind of loop is `while`

```c
while (<condition>) {
  /* Something to do */
}
```

* Do - loop always executes at least once, then checks condition

```c
do {
  /* Something to do */
} while(have_not_won);
```

#### Loop structure

* Do something simple before the loop, like set a counter
* Have a simple test condition on the loop
* Do something at the end of the loop, like update a counter

```c
int counter = 1;
while (counter < 11) {
  printf("%i green bottles, hanging on a wall\n", counter);
  counter --;
}
```

* `For` loop makes it easy:

```c
int counter;
for (counter = 1; counter < 11; counter++) {
  printf("%i green bottles, hanging on a wall\n", counter);
}
```

#### Break and Continue

* To escape from the loop from the middle, you use the `break`
  statement (`break` will not break an `if` statement):

```c
while(feeling_hungry) {
  eat_cake();
  if (feeling_queasy) {
    break;
  }
  drink_coffee();
}
```

* To skip the rest of the loop body and go back to the start, use
  `continue` statement:

```c
while(feeling_hungry) {
  if (not_lunch_yet) {
    continue;
  }
  eat_cake();
}
```

#### Writing Functions

* Function structure:

```c
#include <stdio.h>

int larger(int a, int b) {
  if (a > b)
    return a;
  return b;
}

int main() /* Returns int value, named main */
{
  /* Body of the function (part of the function that does stuff) is surrounded by braces */
  int greatest = larger(100, 1000);
  /* Calling the larger() function with parameters */
  printf("%i is the greatest!\n", greatest);
  return 0;
  /* When you're done, return a value */
}
```

#### Void Functions

* `void` functions are functions that have nothing useful to return.
  They do not need a `return` statement in them.
* The `void` keyword means *it doesn't matter*.

```c
void complain() {
  puts("I'm really not happy");
}
```

#### Chaining assignments

* Setting several variables to the same value

```c
y = x = 4;
```

* This code assigns the number 4 to both y and x.

## Chapter 2: Memory and Pointers

#### Pointers

* Pointer: an address of a piece of data in memory.
* Used for two reasons:
  1. Instead of passing around a whole copy of the data, you can just
     pass a pointer.
  2. You might want two pieces of code to work on the same piece of
     data rather than a separate copy.
* Why do people find them confusing? They are a form of indirection. If you're not careful, you can quickly get lost chasing pointers through memory.

#### Digging into Memory

* Every time you declare a variable, the computer creates space for it
  somewhere in memory.
* If you declare it inside a function, the computer will store it in
  a section of memory called the **stack**.
* If you declare it outside a function, the computer will store it in
  a section of memory called the **globals**.

#### C sends arguments as values

```c
#include <stdio.h>

void go_south_east(int lat, int lon) {
  lat = lat - 1;
  lon = lon + 1;
}

int main() {
  int latitude = 32;
  int longitude = -64;
  go_south_east(latitude, longitude);
  printf("Avast! Now at: [%i, %i]\n", latitude, longitude);
  return 0;
}
```

* This code breaks because of the way that C calls functions.
  1. The `main()` function has a local variable called longitude that 
  had value 32.
  2. When the computer calls the `go_south_east()` function, it copies
     the value of the longitude variable to the lon argument. When you
     call a function, you don't send the variable as an argument, just
     its value.
  3. When the `go_south_east()` function changes the value of `lon`, 
  the function is changing its local copy. When the computer runs
  `main()`, the longitude variable still has its original value.
* We can fix this with pointers.

#### Passing a pointer to the variable

* Instead of passing the value of `latitude` and `longitude`, just pass
  the addresses.
* If the `go_south_east()` function is told that the `latitude` value
  lives at a location, then it will not only be able to find the
  current value, but it will be able to change the contents of the
  original latitude variable.
* Pointers let functions share memory - data created by one function can
  be modified by another function.

#### Using memory pointers

* Three things you need to know to use pointers:
  1. Get the address of a variable:
  ```c
  int x = 4;
  printf("x lives at %p\n", &x);
  ```
    * Store it to a pointer variable:

    ```c
    int *address_of_x = &x;
    ```

  2. Read the contents of an address:

    ```c
    int value_stored = *address_of_x;
    ```

    * * and & are opposites. & takes a piece of data and tells you
        where it's stored. * takes an address and tells you what's
        stored there.
  3. Change the contents of an address:

  ```c
  *address_of_x = 99;
  ```

#### Passing a string to a function

* Strings in C are arrays of characters

```c
void fortune_cookie(char msg[]) {
  printf("Message reads: %s\n", msg);
}

char quote[] = "Cookies make you fat";
fortune_cookie(quote);
```

#### Array variables are like pointers

* Array variable can be used as a pointer to the start of the array in
  memory
* The computer associates the address of the first character with the
  `quote` variable
* Every time the `quote` variable is used in the code, the computer
  substitutes it with the address of the first character in the 
  string. The array variable is just like a pointer.

```c
printf("The quote string is stored at: %p\n", quote);
```

#### What the computer thinks

```c
void fortune_cookie(char msg[]) {
  printf("Message reads: %s\n", msg);
  printf("msg occupies %i bytes\n", sizeof(msg));
}

char quote[] = "Cookies make you fat";
fortune_cookie(quote);
```

1. Sees the function
  * "Looks like they intend to pass an array to this function."
2. Sees the function contents
  * "I can print the message because I know it starts at location msg."
3. Calls the function
  * "So quote is an array and I have to pass the quote variable to
    fortune_cookie(). I'll set the msg argument to the address where 
    the quote array starts in memory"

#### Array variables aren't quite pointers

* Even though you can use an array as a pointer, there are differences:
1. `sizeof(array)` is the size of an array, rather than 4/8 (size of
   pointers on 32- and 64- bit systems. C knows that you want to know
   how long the array is, and returns the number of characters.
2. The address of the array is just the array - a pointer variable
   stores a memory address, but an array variable doesn't.
3. An array variable can't point anywhere else, because computers don't
   allocate storage for the variable itself, just the value.

#### Getting the user to enter a string from the keyboard

* Using the `scanf()` function

```c
char name[40];
printf("What's your name? ");
scanf("%39s", name);
/* Gets the name */
```

* Entering numbers with `scanf()`

```c
int age;
printf("What's your age? ");
scanf("%i", &age);
```

* Entering more than one piece of information at a time

```c
char first_name[20];
char last_name[20];
printf("What's your first and last name? ");
scanf("%19s %19s", first_name, last_name);
printf("First: %s Last:%s\n", first_name, last_name);
```

#### Be careful with scanf()

* You have to use a limit (`%39s`, `%2s`, etc.) of the number of
  characters that `scanf()` will accept, because there is a limit on
  space allocated to the variable.
* If you do not use a limit, there can be a problem if someone types 
  too much: the program will crash

```c
char drink[5];
printf("What drink do you like? ");
scanf("%s", drink);
printf("You like the drink %s\n", drink);
```

* The C program does not work:

```bash
> ./drink
What drink do you like? covfefe
Favorite drink: covfefe
Segmentation fault: 11
>
```

#### fgets() and scanf()

* The function `fgets()` can be used instead of `scanf()`
* It must be given a maximum length, meaning you can't forget to set
  a maximum length:

```c
char drink[5];
printf("What drink do you like? ");
fgets(drink, sizeof(drink), stdin);
```

#### Using sizeof with fgets()

* Using `sizeof()` is dangerous with pointers because it returns the
  amount of space occupied by a variable
* With pointer variables, `sizeof()` would just returned the size of
  a pointer (4 or 8).
* If you're passing a simple pointer, enter the size you want instead of using `sizeof()`

```c
printf("What drink do you like? ");
fgets(drink, 5, stdin);
```

#### Memory

1. Stack - the section of memory used for local variable storage;
   changeable, starts at the top of memory and grows downward.
2. Heap - for dynamic memory: pieces of data that get created while the
   program runs and then stay around for a while.
3. Globals - lives outside all of the functions and is visible to all 
   of them. Get created when the program first runs, and is visible to
   all of them.
4. Constants - also created when the program first runs, but are stored
   in read-only memory. Things like string literals that you will need
   when the program is running but you'll never want them to change.
5. Code - part of the memory where the actual assembled code gets
   loaded. Also read-only.

![Memory stack diagram](/assets/memorystack.jpg)

## Chapter 2.5: String theory

#### String databases

* To create a C program that searches through a list of strings, you
  have to create an array of arrays.
* You can record several things at once in an array, but each string is
  also an array.

```c
char tracks[][80] = {
  "I left my heart in Harvard Med School",
  "Newark, Newark - a wonderful town",
  "Dancing with a Dork",
  "From here to maternity",
  "The girl from Iwo Jima",
};
```

![an array of arrays](/assets/arrayofarrays.png)

* You can find an individual track name like this:

```c
tracks[4]
```

* This code returns `The girl from Iwo Jima`. Remember, lists start at 0.

* Can read individual characters as well:

```c
tracks[4][6]
```

* This code returns `r`, the seventh character in the fifth string.
  Remember, characters include spaces.

#### How to build a C search library with arrays of arrays

Spec:
1. Ask the user for the text they're looking for
2. Loop through all of the track names
3. If a track name contains the search text, display the track name

What we know:
* How to record the tracks
* How to read the value of an individual track name
* How to ask the user for a piece of text to search for

What we need to know:
* How to look to see if the track name contains a given piece of text

#### Using string.h

* The C Standard Library does useful stuff like opening files, doing
  math, or managing memory.
* Library is broken up into several sections.
* One is `stdio.h`, and another is `string.h`.

```c
#include <stdio.h>
#include <string.h>
```

* `string.h` functions:
  * `strcat()` Concatenate two strings.
  * `strstr()` Find the location of a string inside another string.
  * `strchr()` Find the location of a character inside a string.
  * `strlen()` Find the length of a string.
  * `strcmp()` Compare two strings.
  * `strcpy()` Copy one string to another.

* We can use `strstr()` to create a search program.

#### Using strstr()

* Let's say you're looking for "fun" inside of "dysfunctional":

```c
strstr("dysfunctional", "fun");
```

* Searches for the second string in the first string
* If it finds the string, it will return the address of the located
  string in memory
* If the function can't find the substring, it returns the value 0

```c
char s0[] = "dysfunctional";
char s1[] = "fun";
if (strstr(s0, s1))
  puts("I found the fun in dysfunctional!");
```

#### Search through a list

```c
#include <stdio.h>
#include <string.h>

char tracks[][80] = {
  "I left my heart in Harvard Med School",
  "Newark, Newark - a wonderful town",
  "Dancing with a Dork",
  "From here to maternity",
  "The girl from Iwo Jima",
};

void find_track(char search_for[])
{
  int i;
  for (i = 0; i < 5; i++) {
    if (strstr(tracks[i], search_for)) {
      printf("Track %i: '%s'\n", i, tracks[i]);
    }
  }
}

int main() {
  char search_for[80];
  printf("Search for: ");
  fgets(search_for, 80, stdin);
  if(search_for[strlen(search_for) - 1]=='\n'){
    search_for[strlen(search_for) -1]='\0';
  }
  find_track(search_for);
  return 0;
}
```

#### Array of arrays vs array of pointers

* Instead of using an array of arrays to store a sequence of strings,
  you can use an array of pointers, which is a list of memory addresses
  stored in an array.

```c
char *names_for_dog[] = {"Bowser", "Bonza", "Snodgrass"};
```

* You can access the array of pointers just like you accessed the array
  of arrays.

## Chapter 3: Creating small tools

* Every operating system includes small tools

#### Small tools can solve big problems

* A **small tool** is a program that does one task incredibly well.
* Most OSs come with many small tools that you can run from the
  terminal.
* The program needs to read data from a file instead of from the
  keyboard.
* You can use STDIN - standard input and standard output.

#### You can use redirection

* `scanf()` and `printf()` are created by the operating system 
  when the program runs.
* These functions read and write Standard Input and Standard 
  Output.
* You can redirect the standard input and output so that they
  read and write data somewhere else (i.e. to and from files).

#### Redirect Stdin with <

* The `<` operator tells the operating system that the Standard Input of the program should be connected to the data file instead of the keyboard. So you can send the program data **from** a file. Now you just need to redirect its output.

```bash
> ./geo2json < gpsdata.csv
data=[
{ ... }
]
>
```

#### Redirect Standard Output with >

* The `>` operator redirects the Standard Output to a file.

```bash
> ./geo2json < gpsdata.csv > output.json
>
```

* This redirects the output and, instead of displaying it on the 
  screen, it places it in `output.json`.

#### Check error status if you are redirecting the output

* If the program finds a problem in the data, it exits with a status of
  2.
* To check the error status after the program has finished, use:

```bash
echo $?
```

#### Standard Error

* A special output for errors
* Second output created for sending error messages
* Humans have two ears and one mouth, while the process has one ear and
  two mouths.
* Sent to the display by default
* If someone redirects the standard input and standard output to file, the standard error will continue to send data to the display.

```c
printf("I like Turtles!");

fprintf(stdout, "I like Turtles!");
```

* These two statements are identical.
* You can tell `fprintf()` to send text to `stdout` or `stderr`

```c
fprintf(stderr, "Invalid longitude: %f\n", longitude);
```

* This sends the invalid longitude to the display rather than to the
  file.

#### Small tools are flexible

* If you write a program that does one thing really well, chances are
  that you will be able to use it in lots of contexts.

#### Connect small tools

* A "pipe" can connect the stdout to the stdin.
* The operating system will handle the details of exactly how the pipe
  will do this.

```bash
bermuda | geo2json
```

#### Outputs to more than one file

* If you need to create another tool that will read data from a single
  file and split it into other files (i.e. sorting), you must create
  other data streams.
* Each data stream is represented by a pointer to a file. You can reate
  a new data stream with `fopen()`.

```c
FILE *in_file = fopen("input.txt", "r");

FILE *out_file = fopen("output.txt", "w");
```

* This creates a data stream to read from a file, and then a data 
  stream to write to a file.
* The mode is set in the second parameter as "w" (write), "r" (read), 
  or "a" (append).
* Once you've created a data stream, you can print to it using
  `fprintf()` or read from it using `fscanf()`.

```c
fprintf(out_file, "Don't wear %s with %s", "red", "green");

fscanf(in_file, "%79[^\n]\n", sentence);
```

* When you're finished with a data stream, you have to use `fclose()` 
  to close it:

```c
fclose(in_file);
fclose(out_file);
```

#### You can do more with main()

* Any program you will write needs to give the user the ability to
  change the way it works.
* How to read command-line arguments from within the program?
* You can use arguments with the `main()` function:

```c
int main(int argc, char *argv[])
{
  /* The code */
}
```

```bash
./categorize mermaid mermaid.csv Elvis elvises.csv the_rest.csv
```

* The `main()` function reads the command-line args as an array of
  strings.
* The `argc` value is a count of the number of elements in the array

#### Special library function

* You can use a special library function called `getopt()` to deal with
  command-line options.
* Whenever you call `getopt()`, it returns the next option it finds on
  the command line.
* The program needs an option that will take a value and another that
  is *on* or *off*.

```c
#include <unistd.h>

...
while((ch = getopt(argc, argv, "ae:")) !=EOF)
  switch(ch) {
    ...
    case 'e':
      engine_count = optarg;
      ...
  }
argc -=optind;
argv += optind;
```

* This uses a `switch` statement to handle each of the valid options.
* `ae:` tells `getopt()` function that `a` and `e` are valid options.
  This is followed by a colon to tell the function that -e needs to be
  followed by an extra argument, which `getopt()` will point to with 
  the `optarg` variable.
* When the loop finishes, you tweak `argv` and `argc` to skip past all
  the arguments and get to the main command line arguments.

## Chapter 4: Break it down, build it up

#### A guide to data types

* `char`: a character that is stored in the computer's memory as
  a character code.
* `int`: a number that is at least 16 bits and can store numbers up to
  a few million.
* `short`: a number that takes up about half the space of an `int` and is
  for numbers up to a few hundreds.
* `long`: a number that takes up double the space of an `int` and can
  hold numbers up to several billion.
* `float`: the basic data type for storing floating-point numbers.
  * floating-point numbers: numbers without a fixed number of digits
    before and after the decimal point.
* `double`: takes up twice the memory as a float, and stores numbers that
  are larger and more precise than a `float`.

#### A program to help workers bus tables

```c
#include <stdio.h>

float total = 0.0;
short count = 0;
/* This is 6%. Which is a lot less than my agent takes...*/
short tax_percent = 6;

int main()
{
  /* Hey - I was up for a movie with Val Kilmer */
  float val;
  printf("Price of item: ");
  while (scanf("%f", &val) == 1) {
    printf("Total so far: %.2f\n", add_with_tax(val));
    printf("Price of item: ");
  }
  printf("\nFinal total: %.2f\n", total);
  printf("Number of items: %hi\n", count);
  return 0;
}

float add_with_tax(float f)
{
  float tax_rate = 1 + tax_percent / 100.0;
  /* And what about the tip? Voice lessons ain't free */
  total = total + (f * tax_rate);
  count = count + 1;
  return total;
}
```

#### Compilers don't like surprises.

```c
printf("Total so far: %.2f\n", add_with_tax(val));
```

* When the compiler sees this line of code, this happens:
  1. The compiler sees a call to a function it doesn't recognise.
  2. The compiler needs to know what data type the function will return
  3. When it reaches the function's code, it returns a "conflicting
     types for 'add_with_tax'" error.
    * This is because the compiler has two functions with the same 
    name.
* Instead, you have to define the function **before** you call it in
  `main()`.

#### Split the declaration from the definition

* Put a list of function declarations at the start of the C program
  and then you can use them in any order later.

```c
#include <stdio.h>

float do_something_fantastic();
double awesomeness_2_dot_0();
int stinky_pete();
char make_maguerita(int count);

int main() {
  ...
}
```

* Even better, you can just take this list and put them in a header
  file.

#### Creating header files

* Create a header file called `name.h`
* Implement it into the C program with double quotes:

```c
#include <stdio.h>
#include "name.h"
```

* Inside the header file, put the list of function declarations.

#### Reusing common features

```c
void encrypt(char *message)
{
  char c;
  while(*message) {
    *message = *message ^ 31;
    message++;
  }
}
```

* Split the code into a separate file

* `encrypt.h`:

```c
void encrypt(char *message);
```

* `encrypt.c`:

```c
#include <stdio.h>
#include "encrypt.h"

void encrypt(char *message){
  char c;
  while(*message) {
    *message = *message ^ 31;
    message ++;
  }
}
```

* `message_hider.c`:

```c
#include <stdio.h>
#include "encrypt.h"

int main() {
  char msg[80];
  while (fgets(msg, 80, stdin)) {
    encrypt(msg);
    printf("%s", msg);
  }
}
```

* The `encrypt.h` allows the programs to be linked, allowing the
  `message_hider.c` file to use the `encrypt.c` file:

```bash
gcc message_hider.c encrypt.c -o message_hider
./message_hider
>
```

##### ARDUINO IDE LAB

## Chapter 5: Roll your own structures
